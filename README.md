# dotfiles
Personal dotfiles for linux account. In particular for:

    - bash
    - dircolors
    - git
    - XCompose (multi-key input sequences)
